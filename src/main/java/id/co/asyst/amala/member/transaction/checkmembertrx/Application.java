package id.co.asyst.amala.member.transaction.checkmembertrx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;


@SpringBootApplication
@ImportResource( {"transaction-checkmembertrx-service.xml", "beans.xml"} )
//@ImportResource( {"transaction-checkmembertrx-queue.xml", "beans.xml"} )
//@ImportResource( {"transaction-checkmembertrx-queue-receiver.xml", "beans.xml"} )
//@ImportResource( {"transaction-checkmembertrx-queue-receiver-memberid.xml", "beans.xml"} )
public class Application {

    public static void main(String[] args) throws Exception{
    	
        SpringApplication.run(Application.class, args);
    }
}