package id.co.asyst.amala.member.transaction.checkmembertrx.utils;

import com.google.gson.Gson;
import id.co.asyst.amala.member.transaction.checkmembertrx.utils.model.Checkmembertrx;
import id.co.asyst.commons.utils.constant.CommonsConstants;
import id.co.asyst.commons.utils.formatter.Responses;
import org.apache.camel.Exchange;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.*;

public class Utils {


	private static final Logger log = LogManager.getLogger(Responses.class);

	private static final Gson gson = new Gson();

	public void validateRequest(Exchange exchange){
		String memberid = exchange.getProperty("memberid").toString();
		String tierid = exchange.getProperty("tierid").toString();
		String statusValidation = "";
		String statusValidationMsg = "";
		String processBy = "";
		if (StringUtils.isEmpty(memberid) && StringUtils.isEmpty(tierid)){
			statusValidation = "failed";
			statusValidationMsg = "Tier ID is null";
		} else {
			if (!StringUtils.isEmpty(tierid)){
				statusValidation = "next";
				processBy = "tierid";
			} else {
				statusValidation = "next";
				processBy = "memberid";
			}
		}
		exchange.setProperty("statusValidation", statusValidation);
		exchange.setProperty("statusValidationMsg",statusValidationMsg);
		exchange.setProperty("processBy",processBy);
	}

	public void generateDateTimeNow(Exchange exchange){
		Date date = new Date();
		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String srtDate = sm.format(date);
		exchange.setProperty("srtDate",srtDate);
	}

	public void generateMonitoringid(Exchange exchange){
		Date date = new Date();
		String monitoringid = GenerateId("",date,8);
		exchange.setProperty("monitoringid", monitoringid);
	}

	public void generateFixerHeaderID(Exchange exchange){
		Date date = new Date();
		String memberid = exchange.getProperty("memberid").toString();
		String fixerheaderid = GenerateId(memberid,date,8);
		exchange.setProperty("fixerheaderid", fixerheaderid);
	}

	public static String GenerateId(String prefix, Date date, int numdigit) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
		Random rand = new Random();
		String dateFormat = "";
		if (date != null) {
			dateFormat = sdf.format(date);
		}

		String[] arr = new String[]{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
		String randomkey = "";

		for(int i = 0; i < numdigit; ++i) {
			int random = rand.nextInt(36);
			randomkey = randomkey + arr[random];
		}

		return prefix + dateFormat + randomkey;
	}

	public void getResultFromListData(Exchange exchange){
		List<Map<String, Object>> trxDetail = exchange.getProperty("total", List.class);
		exchange.setProperty("total", trxDetail.get(0).get("total"));
	}

	public void setStatus(Exchange exchange){
		String status = "";
		int statusStatus = 0;
		int earningAmala = Integer.parseInt(exchange.getProperty("earningAmala").toString());
		int spendingAmala = Integer.parseInt(exchange.getProperty("spendingAmala").toString());
		int retroclaimAmala = Integer.parseInt(exchange.getProperty("retroclaimAmala").toString());
		int cancellationAmala = Integer.parseInt(exchange.getProperty("cancellationAmala").toString());
		int earningMigration = Integer.parseInt(exchange.getProperty("earningMigration").toString());
		int spendingMigration = Integer.parseInt(exchange.getProperty("spendingMigration").toString());
		int retroclaimMigration = Integer.parseInt(exchange.getProperty("retroclaimMigration").toString());
		int earningAwardmiles = Integer.parseInt(exchange.getProperty("earningAwardmiles").toString());
		int earningTiermiles = Integer.parseInt(exchange.getProperty("earningTiermiles").toString());
		int earningFrequency = Integer.parseInt(exchange.getProperty("earningFrequency").toString());
		int cancellationAward = Integer.parseInt(exchange.getProperty("cancellationAward").toString());
		int cancellationTier = Integer.parseInt(exchange.getProperty("cancellationTier").toString());
		int cancellationFrequency = Integer.parseInt(exchange.getProperty("cancellationFrequency").toString());
		int earningAwardMigration = Integer.parseInt(exchange.getProperty("earningAwardMigration").toString());
		int earningTierMigration = Integer.parseInt(exchange.getProperty("earningTierMigration").toString());
		int earningFrequencyMigration = Integer.parseInt(exchange.getProperty("earningFrequencyMigration").toString());
		status += setStatus(earningAmala, earningMigration, "EARNING", status.length());
		status += setStatus(spendingAmala, spendingMigration, "SPENDING", status.length());
		status += setStatus(retroclaimAmala, retroclaimMigration, "RETROCLAIM", status.length());
		status += setStatus(earningAwardmiles, earningAwardMigration, "MILES", status.length());
		if (cancellationAmala > 0) {
			if (status.length() > 0)
				status += " or CANCELLATION";
			else
				status += "CANCELLATION";
		}
		exchange.setProperty("status", status);
	}

	private String setStatus(int amala, int integration, String type, int status){
		String statusString = "";
		if (status > 0) {
			if (amala > integration) {
				statusString = " or "+type+"_MORE";
				return statusString;
			} else if (amala < integration) {
				statusString = " or "+type+"_LESS";
				return statusString;
			} else{
				statusString = " or "+type+"_BALANCE";
				return statusString;
			}
		} else {
			if (amala > integration) {
				statusString = type+"_MORE";
				return statusString;
			} else if (amala < integration) {
				statusString = type+"_LESS";
				return statusString;
			} else{
				statusString = type+"_BALANCE";
				return statusString;
			}
		}
	}

	/* setData response success */
	public void setResponseSuccess(Exchange exchange) {
		Map<String, Object> response = new HashMap<String, Object>();
		Checkmembertrx checkmembertrx = new Checkmembertrx();
		checkmembertrx.setFixerheaderid(exchange.getProperty("fixerheaderid").toString());
		checkmembertrx.setMemberid(exchange.getProperty("memberid").toString());
		checkmembertrx.setAmalaearning(Integer.parseInt(exchange.getProperty("earningAmala").toString()));
		checkmembertrx.setAmalaretro(Integer.parseInt(exchange.getProperty("retroclaimAmala").toString()));
		checkmembertrx.setAmalacancellation(Integer.parseInt(exchange.getProperty("cancellationAmala").toString()));
		checkmembertrx.setMigrationearning(Integer.parseInt(exchange.getProperty("earningMigration").toString()));
		checkmembertrx.setMigrationretro(Integer.parseInt(exchange.getProperty("retroclaimMigration").toString()));
		checkmembertrx.setAmalaawardmiles(Integer.parseInt(exchange.getProperty("earningAwardmiles").toString()));
		checkmembertrx.setAmalatiermiles(Integer.parseInt(exchange.getProperty("earningTiermiles").toString()));
		checkmembertrx.setAmalafrequency(Integer.parseInt(exchange.getProperty("earningFrequency").toString()));
		checkmembertrx.setAmalacancelaward(Integer.parseInt(exchange.getProperty("cancellationAward").toString()));
		checkmembertrx.setAmalacanceltier(Integer.parseInt(exchange.getProperty("cancellationTier").toString()));
		checkmembertrx.setAmalacancelrrequency(Integer.parseInt(exchange.getProperty("cancellationFrequency").toString()));
		checkmembertrx.setMigrationawardmiles(Integer.parseInt(exchange.getProperty("earningAwardMigration").toString()));
		checkmembertrx.setMigrationtiermiles(Integer.parseInt(exchange.getProperty("earningTierMigration").toString()));
		checkmembertrx.setMigrationfrequency(Integer.parseInt(exchange.getProperty("earningFrequencyMigration").toString()));
		checkmembertrx.setStatus(exchange.getProperty("status").toString());
		checkmembertrx.setCreatedby("SYSTEM");
		checkmembertrx.setCreateddate(exchange.getProperty("srtDate").toString());
		checkmembertrx.setAmalaredeem(Integer.parseInt(exchange.getProperty("spendingAmala").toString()));
		checkmembertrx.setMigrationredeem(Integer.parseInt(exchange.getProperty("spendingMigration").toString()));

		response.put(CommonsConstants._RESPONSE_STATUS, status(exchange));
		response.put(CommonsConstants._SERVICE_IDENTITY_KEY, exchange.getProperty("identity", Map.class));
		response.put(CommonsConstants._RESPONSE_RESULT_KEY, checkmembertrx);
		exchange.getOut().setBody(gson.toJson(response));
	}

	@SuppressWarnings("unused")
	private Map<String,Object> status(Exchange exchange){
		Map<String,Object> status=new HashMap<String,Object>();
		String rescode=exchange.getProperty("rescode",String.class);
		String resdesc=exchange.getProperty("resdesc",String.class);
		String resmsg=exchange.getProperty("resmsg",String.class);


		status.put(CommonsConstants._RESPONSE_CODE,rescode);
		status.put(CommonsConstants._RESPONSE_DESC,resdesc);
		status.put(CommonsConstants._RESPONSE_MSG,resmsg);

		return status;
	}
}
