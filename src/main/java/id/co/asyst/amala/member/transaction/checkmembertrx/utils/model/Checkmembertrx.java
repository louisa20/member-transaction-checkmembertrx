package id.co.asyst.amala.member.transaction.checkmembertrx.utils.model;

public class Checkmembertrx {
    String fixerheaderid;
    String memberid;
    int amalaearning;
    int amalaretro;
    int amalacancellation;
    int migrationearning;
    int migrationretro;
    int amalaawardmiles;
    int amalatiermiles;
    int amalafrequency;
    int amalacancelaward;
    int amalacanceltier;
    int amalacancelrrequency;
    int migrationawardmiles;
    int migrationtiermiles;
    int migrationfrequency;
    int amalaredeem;
    int migrationredeem;
    String status;
    String createdby;
    String createddate;

    public int getAmalaredeem() {
        return amalaredeem;
    }

    public void setAmalaredeem(int amalaredeem) {
        this.amalaredeem = amalaredeem;
    }

    public int getMigrationredeem() {
        return migrationredeem;
    }

    public void setMigrationredeem(int migrationredeem) {
        this.migrationredeem = migrationredeem;
    }

    public String getFixerheaderid() {
        return fixerheaderid;
    }

    public void setFixerheaderid(String fixerheaderid) {
        this.fixerheaderid = fixerheaderid;
    }

    public String getMemberid() {
        return memberid;
    }

    public void setMemberid(String memberid) {
        this.memberid = memberid;
    }

    public int getAmalaearning() {
        return amalaearning;
    }

    public void setAmalaearning(int amalaearning) {
        this.amalaearning = amalaearning;
    }

    public int getAmalaretro() {
        return amalaretro;
    }

    public void setAmalaretro(int amalaretro) {
        this.amalaretro = amalaretro;
    }

    public int getAmalacancellation() {
        return amalacancellation;
    }

    public void setAmalacancellation(int amalacancellation) {
        this.amalacancellation = amalacancellation;
    }

    public int getMigrationearning() {
        return migrationearning;
    }

    public void setMigrationearning(int migrationearning) {
        this.migrationearning = migrationearning;
    }

    public int getMigrationretro() {
        return migrationretro;
    }

    public void setMigrationretro(int migrationretro) {
        this.migrationretro = migrationretro;
    }

    public int getAmalaawardmiles() {
        return amalaawardmiles;
    }

    public void setAmalaawardmiles(int amalaawardmiles) {
        this.amalaawardmiles = amalaawardmiles;
    }

    public int getAmalatiermiles() {
        return amalatiermiles;
    }

    public void setAmalatiermiles(int amalatiermiles) {
        this.amalatiermiles = amalatiermiles;
    }

    public int getAmalafrequency() {
        return amalafrequency;
    }

    public void setAmalafrequency(int amalafrequency) {
        this.amalafrequency = amalafrequency;
    }

    public int getAmalacancelaward() {
        return amalacancelaward;
    }

    public void setAmalacancelaward(int amalacancelaward) {
        this.amalacancelaward = amalacancelaward;
    }

    public int getAmalacanceltier() {
        return amalacanceltier;
    }

    public void setAmalacanceltier(int amalacanceltier) {
        this.amalacanceltier = amalacanceltier;
    }

    public int getAmalacancelrrequency() {
        return amalacancelrrequency;
    }

    public void setAmalacancelrrequency(int amalacancelrrequency) {
        this.amalacancelrrequency = amalacancelrrequency;
    }

    public int getMigrationawardmiles() {
        return migrationawardmiles;
    }

    public void setMigrationawardmiles(int migrationawardmiles) {
        this.migrationawardmiles = migrationawardmiles;
    }

    public int getMigrationtiermiles() {
        return migrationtiermiles;
    }

    public void setMigrationtiermiles(int migrationtiermiles) {
        this.migrationtiermiles = migrationtiermiles;
    }

    public int getMigrationfrequency() {
        return migrationfrequency;
    }

    public void setMigrationfrequency(int migrationfrequency) {
        this.migrationfrequency = migrationfrequency;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedby() {
        return createdby;
    }

    public void setCreatedby(String createdby) {
        this.createdby = createdby;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }
}
