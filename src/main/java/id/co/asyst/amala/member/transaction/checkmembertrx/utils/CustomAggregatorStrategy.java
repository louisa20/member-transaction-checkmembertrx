package id.co.asyst.amala.member.transaction.checkmembertrx.utils;

import org.apache.camel.Exchange;
import org.apache.camel.processor.aggregate.AggregationStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Ahmad Dimas Abid Muttaqi
 * @version $Revision$, Feb 14, 2019
 * @since 1.0
 */
public class CustomAggregatorStrategy implements AggregationStrategy {

    @Override
    public Exchange aggregate(Exchange oldExchange, Exchange newExchange) {
        Map<String,Object> transactionDetail = newExchange.getProperty("piecestransactiondetail", Map.class);
        List<Map<String, Object>> transactionDetailList = null;

        if (transactionDetail != null) {
            if (oldExchange == null) {
                transactionDetailList = new ArrayList<>();
                transactionDetailList.add(transactionDetail);
            } else {
                transactionDetailList = oldExchange.getProperty("transactiondetail", List.class);
                transactionDetailList.add(transactionDetail);
            }
        }

        newExchange.setProperty("transactiondetail", transactionDetailList);
        return newExchange;
    }
}
